# -*- coding: utf-8 -*-
"""
Created on Tue Apr 21 19:27:02 2020

@author: Anu
"""
#importing the required libraries
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from sklearn import metrics


#reading the csv file
LR_df = pd.read_excel('D:\INTROTALLENT\Python\Project-datasets\Global Superstore.xls')

#checking the type of data present in the dataset
LR_df.dtypes
LR_df.shape

# checking for null values
LR_df.isnull().sum()

"""
crisp-dm implementation

"""

"""
1. data understanding
"""

# pairplot to see how the data is panned
sns.pairplot(LR_df)


# Density plot with histograms of column values for each numerical column 
# before outlier treatment

#sales column
sns.distplot(LR_df["Sales"], hist=True, kde=True, 
             bins=int(10), color = 'darkblue', 
             hist_kws={'edgecolor':'black'},
             kde_kws={'linewidth': 2})

#Quantity column
sns.distplot(LR_df["Quantity"], hist=True, kde=True, 
             bins=int(11), color = 'darkblue', 
             hist_kws={'edgecolor':'black'},
             kde_kws={'linewidth': 2})

#Shipping Cost column
sns.distplot(LR_df["Shipping Cost"], hist=True, kde=True, 
             bins=int(15), color = 'green', 
             hist_kws={'edgecolor':'black'},
             kde_kws={'linewidth': 2})


# to check how sales depends on other columns
#sales vs quantity graph
sns.regplot(x=LR_df["Sales"], y=LR_df["Quantity"], fit_reg=False)

#sales vs Category graph
sns.regplot(x=LR_df["Sales"], y=LR_df["Category"], fit_reg=False)

#sales vs Sub-Category graph
sns.regplot(x=LR_df["Sales"], y=LR_df["Sub-Category"], fit_reg=False)


#sales vs Shipping Cost graph
sns.regplot(x=LR_df["Sales"], y=LR_df["Shipping Cost"], fit_reg=False)


#sales vs Order Priority graph
sns.regplot(x=LR_df["Sales"], y=LR_df["Order Priority"], fit_reg=False)

#sales vs Market graph
sns.regplot(x=LR_df["Sales"], y=LR_df["Market"], fit_reg=False)

#sales vs region graph
sns.catplot(x="Sales", y="Region", data=LR_df);

#sales vs Ship Mode graph
sns.catplot(x="Sales", y="Ship Mode", data=LR_df);

# sns.catplot(x="Sales", y="Region", kind="bar", data=LR_df);
# sns.catplot(x="Sales", y="Market", kind="bar", data=LR_df);



"""
2. data preparation
"""


#to deal with null values in the columns

# filling the null values with median in shipping cost col
median_value=LR_df['Shipping Cost'].median()
LR_df['Shipping Cost']=LR_df['Shipping Cost'].fillna(median_value)

# since postal code has definite values, the nulls are filled with 0
LR_df['Postal Code'] = LR_df['Postal Code'].replace(np.nan, 0)

LR_df['Region'] = LR_df['Region'].replace(np.nan, 'no region')
LR_df['Category'] = LR_df['Category'].replace(np.nan, 'no category')

LR_df['Sub-Category'] = LR_df['Sub-Category'].replace(np.nan, 'no sub-category')

#


#changing order and ship date datatype to object
LR_df['Order Date'] = LR_df['Order Date'].apply(lambda x: x.strftime('%Y-%m-%d'))
LR_df['Ship Date'] = LR_df['Ship Date'].apply(lambda x: x.strftime('%Y-%m-%d'))

'''
null values in the categorical columns are left alone
those values is going to be empty
'''

'''
outlier treatment for the numerical colmns
'''

# def to check the outliers in the df
def ro(d,c):
    q1=d[c].quantile(0.25)
    q3=d[c].quantile(0.75)
    iqr=q3-q1
    ub=q3+(1.58*iqr)
    lb=q1-(1.58*iqr)
    f1=d[c].quantile(0.05)
    f2=d[c].quantile(0.95)
    d[c][d[c]<lb]=f1
    d[c][d[c]>ub]=f2
    return d[c]

ro(LR_df, 'Sales')
plt.boxplot(LR_df['Sales'])
LR_df.Sales.unique()
LR_df['Sales'].quantile(0.893)
LR_df['Sales'][LR_df['Sales'] > LR_df['Sales'].quantile(0.888)] = LR_df['Sales'].quantile(0.888)


ro(LR_df, 'Quantity')
plt.boxplot(LR_df['Quantity'])
LR_df.Quantity.unique()

ro(LR_df, 'Shipping Cost')
plt.boxplot(LR_df['Shipping Cost'])
LR_df['Shipping Cost'].quantile(0.88)
LR_df['Shipping Cost'][LR_df['Shipping Cost'] > LR_df['Shipping Cost'].quantile(0.88)] = LR_df['Shipping Cost'].quantile(0.88)


'''
3. modelling
'''
#steps implemented to encode the obj columns and separate numeric columns
y = LR_df.iloc[:,:]
X = LR_df.iloc[:,:]

#X1 and X both have dataset X
X1=X
X1

# X_numeric_col has numerical values of X
numerics = ['int64','float64']
X_numeric_col = X1.select_dtypes(include=numerics)
X_numeric_col.dtypes

# X_obj_col has non numerical values of X 
X_obj_col = X1.select_dtypes(include=object)
X_obj_col.dtypes

#transfer all non numerical to numerical- use label encoding on X4
from sklearn import preprocessing

le = preprocessing.LabelEncoder()
X4 = X_obj_col.apply(le.fit_transform)
X4


#combine cleaned data 
X_clean_data=X4.join(X_numeric_col)
X_clean_data


'''
sales column is our important variable
we are analysing the data against the sales column
considering it as the predictor variable
hence the y df has the sales coumn
'''
y = y.Sales


#split data into train n test
from sklearn.model_selection import train_test_split
x_train,x_test,y_train,y_test=train_test_split(X_clean_data,y,test_size=0.3)


print(x_train.shape,y_train.shape)
print(x_test.shape,y_test.shape)


'''
machine learning algorithm implementation
algorithm- linear regression
'''

#fit the model
from sklearn import linear_model
lm=linear_model.LinearRegression()
model=lm.fit(x_train,y_train)

'''
4. Evaluation
'''

prediction_x_test=lm.predict(x_test)
prediction_x_test

prediction_x_train=lm.predict(x_train)
prediction_x_train


train_acc = model.score(x_train,y_train)
print('accuracy:', train_acc)
# accuracy: 1.0


test_acc = model.score(x_test,y_test)
print('accuracy:', test_acc)
# accuracy: 1.0

intercept = model.intercept_
print('intercept:', model.intercept_)
coeff = model.coef_
print('slope:', model.coef_)

print('Mean Absolute Error:', metrics.mean_absolute_error(y_test, prediction_x_test))  
print('Mean Squared Error:', metrics.mean_squared_error(y_test, prediction_x_test))  


# Mean Absolute Error: 1.8255356715902947e-13
# Mean Squared Error: 5.398876258888112e-26


#graphs after the implementation of the algorithm
#histograms
sns.distplot(x_train["Sales"], hist=True, kde=True, 
             bins=int(10), color = 'darkblue', 
             hist_kws={'edgecolor':'black'},
             kde_kws={'linewidth': 2})

sns.distplot(y_test, hist=True, kde=True, 
             bins=int(10), color = 'darkblue', 
             hist_kws={'edgecolor':'black'},
             kde_kws={'linewidth': 2})

'''
# hypothesis testing

1. students t-test
'''

from scipy.stats import ttest_1samp

tset, pval = ttest_1samp(LR_df['Sales'], np.mean(LR_df['Sales']))
print('p-values',pval)

if pval < 0.05:    # alpha value is 0.05 or 5%
   print(" we are rejecting null hypothesis")
else:
  print("we are accepting null hypothesis")

'''
p-values 0.9999999999886471
# we are accepting null hypothesis
'''


'''
chi squared 
'''
from scipy.stats import chi2_contingency

stat, p, dof, expected = chi2_contingency(LR_df['Sales'])
print('stat=%.3f, p=%.3f' % (stat, p))

if p > 0.05:
	print('Probably independent')
else:
	print('Probably dependent')

'''
stat=0.000, p=1.000
Probably independent

'''

'''
co relation test
'''
from scipy.stats import pearsonr
# seed random number generator
# prepare data
# calculate Pearson's correlation

corr, _ = pearsonr(LR_df['Sales'], LR_df['Shipping Cost'])
print('Pearsons correlation: %.3f' % corr)


# Pearsons correlation: 0.890

from scipy.stats import spearmanr
# seed random number generator
# calculate spearman's correlation

corrspr, cor2spr = spearmanr(LR_df['Sales'], LR_df['Shipping Cost'])
print('Spearmans correlation: %.3f' % corrspr)

# Spearmans correlation: 0.911