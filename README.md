# Take on global superstore

**Analytics project with the implementation of Linear regression**

#Data understanding

**Global Super Store data set**-
* Which has around 50k+ values. Its a customer centric data set , which has the data of all the orders that have been placed through different segment and markets, regions and the type of order priority the goods were delivered with considerate shipping cost for that particular quantity.
* The dataset has details about customers like- name, id, city, state, country and the region.
* Details about the products- id, product sold, quantity sold, its ship mode, its shipping cost and priority.
* The dataset also keeps track of the order date and shipping date.
* Our dataset has an interesting column where it tells us what kind of user bought the product ie, the segment (corporate, home or consumer).

**Implementation**
* The dataset is studied and implemented with the help of crisp framework.
* Thorough analysis of the data is done by data visualization after data cleaning.
* The Linear regression algorithm is implemented.
* Hypothesis testing: Chi-squared, Students T-test, correlation testing has been implemented
